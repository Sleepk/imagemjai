package DisplayJAISincronizada;

import com.sun.media.jai.widget.DisplayJAI;
import java.awt.GridLayout;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.image.RenderedImage;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class DisplayTwoSynchronizedImages extends JPanel implements AdjustmentListener {

    /**
	 * 
	 */
	private static final long serialVersionUID = 6193883050844450373L;
	protected DisplayJAI dj1;
    protected DisplayJAI dj2;
    protected JScrollPane jsp1;
    protected JScrollPane jsp2;

    public DisplayTwoSynchronizedImages(RenderedImage im1, RenderedImage im2) {

        super();
        // Cria componente com duas imagens com JScrollPane
        setLayout(new GridLayout());
        dj1 = new DisplayJAI(im1);
        dj2 = new DisplayJAI(im2);
        jsp1 = new JScrollPane(dj1);
        jsp2 = new JScrollPane(dj2);
        add(jsp1);
        add(jsp2);
    }

    @Override
    public void adjustmentValueChanged(AdjustmentEvent e) {

        if (e.getSource() == jsp1.getHorizontalScrollBar()) {
            jsp2.getHorizontalScrollBar().setValue(e.getValue());
        }
        if (e.getSource() == jsp1.getVerticalScrollBar()) {
            jsp2.getVerticalScrollBar().setValue(e.getValue());
        }
        if (e.getSource() == jsp2.getHorizontalScrollBar()) {
            jsp1.getHorizontalScrollBar().setValue(e.getValue());
        }
        if (e.getSource() == jsp2.getVerticalScrollBar()) {
            jsp1.getVerticalScrollBar().setValue(e.getValue());
        }
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
