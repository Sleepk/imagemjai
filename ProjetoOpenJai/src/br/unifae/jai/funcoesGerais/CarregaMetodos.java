package br.unifae.jai.funcoesGerais;

import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.ComponentColorModel;
import java.awt.image.DataBuffer;
import java.awt.image.RenderedImage;
import java.awt.image.WritableRaster;
import java.awt.image.renderable.ParameterBlock;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.media.jai.IHSColorSpace;
import javax.media.jai.ImageLayout;
import javax.media.jai.InterpolationBilinear;
import javax.media.jai.InterpolationNearest;
import javax.media.jai.JAI;
import javax.media.jai.KernelJAI;
import javax.media.jai.PlanarImage;
import javax.media.jai.iterator.RandomIter;
import javax.media.jai.iterator.RandomIterFactory;
import javax.swing.JOptionPane;

public class CarregaMetodos {

	public int contarPixel(String caminho) throws IOException {
		File f = new File(caminho);
		BufferedImage image = ImageIO.read(f);
		RandomIter iterator = RandomIterFactory.create(image, null);

		int[] pixel = new int[3];
		int brancos = 0;

		for (int h = 0; h < image.getHeight(); h++) {
			for (int w = 0; w < image.getWidth(); w++) {
				iterator.getPixel(w, h, pixel);
				if ((pixel[0] == 255) && (pixel[1] == 255) && (pixel[2] == 255)) {
					brancos++;
				}
			}

		}
		return brancos;
	}

	public String dimens�esImagem(String caminho) throws IOException {

		File f = new File(caminho);
		BufferedImage image = ImageIO.read(f);
		return ("Dimens�es da imagem " + image.getWidth() + " x " + image.getHeight() + " pixels");

	}

	public PlanarImage inverterImagem(String caminho) {
		// caminho.replace("\\", "\\\\");
		PlanarImage input = JAI.create("fileload", caminho);
		PlanarImage output = JAI.create("invert", input);
		return output;
	}

	public PlanarImage suavizarImagem(String caminho) {

		PlanarImage imagem = JAI.create("fileload", caminho);
		float[] kernelMatrix = { 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f,
				1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f,
				1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f, 1f / 25f };
		KernelJAI kernel = new KernelJAI(5, 5, kernelMatrix);
		return JAI.create("convolve", imagem, kernel);
	}

	public PlanarImage ditalarImagem(String caminho, int op) {

		PlanarImage imagem = JAI.create("fileload", caminho);
		float[] estrutura = { 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1,
				1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0 };
		KernelJAI kernel = new KernelJAI(7, 7, estrutura);
		ParameterBlock parameterBlock = new ParameterBlock();

		switch (op) {
		case 1:
			// Imagem DILATADA
			parameterBlock.addSource(imagem);
			parameterBlock.add(kernel);
			PlanarImage dilatada = JAI.create("dilate", parameterBlock);
			return dilatada;
		case 2:

			// Imagem ERODIDA
			parameterBlock.addSource(imagem);
			parameterBlock.add(kernel);
			return JAI.create("erode", parameterBlock);

		}
		return imagem;
	}

	public PlanarImage bordasHorizontais(String caminho) {
		PlanarImage imagem = JAI.create("fileload", caminho);
		float[] kernelMatrix = { -1, -2, -1, 0, 0, 0, 1, 2, 1 };
		KernelJAI kernel = new KernelJAI(3, 3, kernelMatrix);
		return JAI.create("convolve", imagem, kernel);
	}

	public PlanarImage ihs(String caminho) {
		PlanarImage imagem = JAI.create("fileload", caminho);
		// Converte para o modelo de cores IHS
		IHSColorSpace ihs = IHSColorSpace.getInstance();
		ColorModel modeloIHS = new ComponentColorModel(ihs, new int[] { 8, 8, 8 }, false, false, Transparency.OPAQUE,
				DataBuffer.TYPE_BYTE);
		ParameterBlock pb = new ParameterBlock();
		pb.addSource(imagem);
		pb.add(modeloIHS);
		RenderedImage imagemIHS = JAI.create("colorconvert", pb);
		// Extra�mos as bandas I, H e S.
		RenderedImage[] bandas = new RenderedImage[3];
		for (int band = 0; band < 3; band++) {
			pb = new ParameterBlock();
			pb.addSource(imagemIHS);
			pb.add(new int[] { band });
			bandas[band] = JAI.create("bandselect", pb);
		}
		// Criamos bandas constantes para as bandas I e S
		pb = new ParameterBlock();
		pb.add((float) imagem.getWidth());
		pb.add((float) imagem.getHeight());
		pb.add(new Byte[] { (byte) 255 });
		RenderedImage novaIntensidade = JAI.create("constant", pb);
		pb = new ParameterBlock();
		pb.add((float) imagem.getWidth());
		pb.add((float) imagem.getHeight());
		pb.add(new Byte[] { (byte) 255 });
		RenderedImage novaSaturacao = JAI.create("constant", pb);
		// Juntamos as bandas H e as I e S constantes.
		// Devemos passar um RenderingHint que indica que o modelo de cor IHS ser�
		// usado.
		ImageLayout imageLeyout = new ImageLayout();
		imageLeyout.setColorModel(modeloIHS);
		imageLeyout.setSampleModel(imagemIHS.getSampleModel());
		RenderingHints rendHints = new RenderingHints(JAI.KEY_IMAGE_LAYOUT, imageLeyout);
		pb = new ParameterBlock();
		pb.addSource(novaIntensidade);
		pb.addSource(bandas[1]);
		pb.addSource(novaSaturacao);
		RenderedImage imagemIHSModificada = JAI.create("bandmerge", pb, rendHints);
		// Convertemos de volta para RGB
		pb = new ParameterBlock();
		pb.addSource(imagemIHSModificada);
		pb.add(imagem.getColorModel()); // Imagem original era em RGB!
		return JAI.create("colorconvert", pb);
	}

	public PlanarImage imagemPadrao(String caminho) {
		return JAI.create("fileload", caminho);
	}

	public PlanarImage rotacionaImagem(String caminho, int rota��o) {
		PlanarImage imagem = JAI.create("fileload", caminho);
		float angle = (float) Math.toRadians(rota��o);
		// usamos o centro da imagem para rota��o
		float centerY = imagem.getWidth() / 2f;
		float centerX = imagem.getHeight() / 2f;

		ParameterBlock parameterBlock = new ParameterBlock();
		parameterBlock.addSource(imagem);
		parameterBlock.add(centerX);
		parameterBlock.add(centerY);
		parameterBlock.add(angle);
		parameterBlock.add(new InterpolationBilinear());

		return JAI.create("rotate", parameterBlock);

	}

	public PlanarImage escalaImagem(String caminho, float escala) {
		PlanarImage imagem = JAI.create("fileload", caminho);
		float scale = 0.3f;
		ParameterBlock parameterBlock = new ParameterBlock();
		parameterBlock.addSource(imagem);
		parameterBlock.add(scale);
		parameterBlock.add(scale);
		parameterBlock.add(0.0f);
		parameterBlock.add(0.0f);
		parameterBlock.add(new InterpolationNearest());
		return JAI.create("scale", parameterBlock);
	}

	public void xadrex() {
		int width = 256;
		int height = 256;

		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_BGR);
		WritableRaster raster = image.getRaster();
		int[] cor1 = new int[] { 255, 0, 0 };
		int[] cor2 = new int[] { 0, 0, 255 };

		for (int h = 0; h < height; h++) {
			for (int w = 0; w < width; w++) {
				if ((((w / 32) + (h / 32)) % 2) == 0) {
					raster.setPixel(w, h, cor1);
				} else {
					raster.setPixel(w, h, cor2);
				}
			}
		}

		try {
			ImageIO.write(image, "PNG", new File("xadrez.png"));
			System.out.println("imagem criada");
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Erro ao salvar a Imagem", "Ocorreu um erro",
					JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
		}
	}

	public PlanarImage binarizarImagem(String caminho) {
		PlanarImage imagemBinarizada = JAI.create("fileload", caminho);
		ParameterBlock pb = new ParameterBlock();
		pb.addSource(imagemBinarizada);
		pb.add(127.0);

		try {
			PlanarImage binarizada = JAI.create("binarize", pb);
			return binarizada;
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"Desculpe, esta imagem n�o pode " + "ser binarizada. Certifique-se que seja SingleBanded. \n",
					"Erro", JOptionPane.ERROR_MESSAGE);
			return imagemBinarizada;
		}
	}

}
