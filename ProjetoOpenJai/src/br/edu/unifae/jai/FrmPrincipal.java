package br.edu.unifae.jai;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.media.jai.JAI;
import javax.media.jai.PlanarImage;
import javax.swing.ButtonGroup;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSeparator;
import javax.swing.JSlider;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import DisplayJAISincronizada.DisplayTwoSynchronizedImages;
import br.unifae.jai.funcoesGerais.CarregaMetodos;

public class FrmPrincipal extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 789988874411173445L;

	// filtros para pegar a arquivos com determinadas extensoes.
	// exemplo retirado https://www.youtube.com/watch?v=AGWgL74n0NM
	private FileNameExtensionFilter filter = new FileNameExtensionFilter("Somente arquivos png", "png");
	private FileNameExtensionFilter filter2 = new FileNameExtensionFilter("Somente arquivos jpg", "jpg", "jpeg");

	String imagemAtual;

	PlanarImage carregarItem = null;

	// cria um grupo radiobutton
	private final ButtonGroup rbtGroup = new ButtonGroup();

	private boolean flag = false;

	// To select the path of image file
	private File file;
	// To load the image file
	// To choose file by browsing
	private JFileChooser FileChooser = new JFileChooser();

	private PlanarImage srcImage = null;

	DisplayTwoSynchronizedImages imagem;

	private JInternalFrame ImageFrame;

	private Container contentPane;

	protected CarregaMetodos manipulador;

	private JLabel lblQuantidadeBrancos;

	private JLabel lblPixelBrancos;

	protected int rotacao;

	private JRadioButton rbtRotacionarImagem;

	private JRadioButton rbtEscalaImagem;

	private JSlider slider;

	private JPanel panel;

	public FrmPrincipal() {
		getContentPane().setBackground(Color.LIGHT_GRAY);

		int inset = 50;

		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(inset, inset, screenSize.width - inset * 2, screenSize.height - inset * 2);

		// setSize(814, 605);
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnArquivo = new JMenu("Arquivo");
		menuBar.add(mnArquivo);

		JMenuItem openMenu = new JMenuItem("Abrir");
		openMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent evt) {
				openMenuActionPerformed(evt);
			}
		});
		mnArquivo.add(openMenu);

		JSeparator separator = new JSeparator();
		mnArquivo.add(separator);

		JMenuItem mntmSalvar = new JMenuItem("Salvar");
		mntmSalvar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent evt) {
				saveMenuActionPerformed(evt);
			}

		});
		mnArquivo.add(mntmSalvar);

		JSeparator separator_1 = new JSeparator();
		mnArquivo.add(separator_1);

		JMenuItem mntmSair = new JMenuItem("Sair");
		mnArquivo.add(mntmSair);

		JMenu mnCriarImagem = new JMenu("Criar Imagens");
		menuBar.add(mnCriarImagem);

		JMenuItem mntmCriarImagemXadrez = new JMenuItem("Criar imagem Xadrez");
		mntmCriarImagemXadrez.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				manipulador = new CarregaMetodos();
				manipulador.xadrex();

			}
		});
		mnCriarImagem.add(mntmCriarImagemXadrez);

		JMenuItem mntmBinarizarImage = new JMenuItem("Binarizar Image");
		mntmBinarizarImage.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manipulador = new CarregaMetodos();
				manipulador.binarizarImagem(imagemAtual);
			}
		});
		mnCriarImagem.add(mntmBinarizarImage);
		getContentPane().setLayout(null);

		panel = new JPanel();
		panel.setBounds(0, 0, 399, 546);
		panel.setBorder(new TitledBorder(null, "Escolha Um Item", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		getContentPane().add(panel);

		JRadioButton rbtIHS = new JRadioButton("IHS");
		rbtIHS.setBounds(12, 154, 200, 23);
		rbtIHS.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manipulador = new CarregaMetodos();
				carregaItens(manipulador, 5);
			}
		});

		JRadioButton rbtSuavizarImagem = new JRadioButton("Suavizar Imagem");
		rbtSuavizarImagem.setBounds(12, 70, 200, 23);
		rbtSuavizarImagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manipulador = new CarregaMetodos();
				carregaItens(manipulador, 1);
			}
		});
		rbtGroup.add(rbtSuavizarImagem);

		JRadioButton rbtDilatarImagem = new JRadioButton("Dilatar Imagem");
		rbtDilatarImagem.setBounds(12, 126, 200, 23);
		rbtDilatarImagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manipulador = new CarregaMetodos();
				carregaItens(manipulador, 3);
			}
		});

		JRadioButton rbtBordasHorizontais = new JRadioButton("Bordas Horizontais");
		rbtBordasHorizontais.setBounds(12, 98, 200, 23);
		rbtBordasHorizontais.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manipulador = new CarregaMetodos();
				carregaItens(manipulador, 2);
			}
		});
		rbtGroup.add(rbtBordasHorizontais);
		rbtGroup.add(rbtDilatarImagem);
		rbtGroup.add(rbtIHS);

		JRadioButton rbtInverterImagem = new JRadioButton("Inverter Imagem");
		rbtInverterImagem.setBounds(12, 182, 200, 23);
		rbtInverterImagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manipulador = new CarregaMetodos();
				carregaItens(manipulador, 6);
			}
		});
		rbtGroup.add(rbtInverterImagem);

		rbtRotacionarImagem = new JRadioButton("Rotacionar Imagem");
		rbtRotacionarImagem.setBounds(12, 210, 200, 23);
		rbtGroup.add(rbtRotacionarImagem);
		rbtRotacionarImagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// rotacionar 7
			}
		});
		rbtGroup.add(rbtRotacionarImagem);

		slider = new JSlider();
		slider.setValue(0);
		slider.setBounds(12, 470, 200, 49);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				manipulador = new CarregaMetodos();
				ImageFrame.setTitle("Rotacionar Imagem - Posição:  " + slider.getValue());

				if (rbtRotacionarImagem.isSelected()) {
					carregaItens(manipulador, 7);
					rotacao = slider.getValue();
				} else if (rbtEscalaImagem.isSelected()) {
					carregaItens(manipulador, 8);
					rotacao = slider.getValue();
				}

			}
		});

		lblPixelBrancos = new JLabel("");
		lblPixelBrancos.setBounds(12, 24, 200, 14);

		lblQuantidadeBrancos = new JLabel("");
		lblQuantidadeBrancos.setBounds(12, 49, 342, 14);
		panel.setLayout(null);

		rbtEscalaImagem = new JRadioButton("Escala Imagem");
		rbtEscalaImagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// escala
			}
		});
		rbtEscalaImagem.setBounds(12, 236, 200, 23);
		rbtGroup.add(rbtEscalaImagem);

		JRadioButton rbtImagemErodita = new JRadioButton("Imagem Erodita");
		rbtGroup.add(rbtImagemErodita);
		rbtImagemErodita.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manipulador = new CarregaMetodos();
				carregaItens(manipulador, 4);

			}
		});
		rbtImagemErodita.setBounds(12, 262, 200, 23);
		panel.add(rbtImagemErodita);

		JRadioButton rbtResetarImagem = new JRadioButton("Resetar Imagem");
		rbtGroup.add(rbtResetarImagem);
		rbtResetarImagem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				manipulador = new CarregaMetodos();
				carregaItens(manipulador, 9);
			}
		});
		rbtResetarImagem.setBounds(12, 289, 200, 23);
		panel.add(rbtResetarImagem);

		panel.add(rbtEscalaImagem);
		panel.add(rbtSuavizarImagem);
		panel.add(rbtBordasHorizontais);
		panel.add(rbtDilatarImagem);
		panel.add(rbtIHS);
		panel.add(rbtInverterImagem);
		panel.add(rbtRotacionarImagem);
		panel.add(lblPixelBrancos);
		panel.add(lblQuantidadeBrancos);
		panel.add(slider);

		ImageFrame = new JInternalFrame("Imagem Sincronizada");
		ImageFrame.setBounds(399, 0, 399, 546);
		getContentPane().add(ImageFrame);
		ImageFrame.setResizable(true);
		ImageFrame.setClosable(true);
		ImageFrame.getContentPane().setLayout(null);
		ImageFrame.setVisible(false);
	}

	private void openMenuActionPerformed(java.awt.event.ActionEvent evt) {
		FileChooser.setFileFilter(filter);
		FileChooser.setFileFilter(filter2);
		try {

			int returnVal = FileChooser.showOpenDialog(this);
			if (returnVal == JFileChooser.APPROVE_OPTION) {

				// Initialise file object
				file = FileChooser.getSelectedFile();
				// Display the image
				imagemSincronizada(file);

				JOptionPane.showMessageDialog(FrmPrincipal.this, file);

			}
		} catch (IllegalArgumentException e) {
			throw new IllegalArgumentException("teste");
		}

	}

	private void saveMenuActionPerformed(java.awt.event.ActionEvent evt) {
		FileChooser.setFileFilter(filter);
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Salvar");
		int userSelection = fileChooser.showSaveDialog(null);

		if (userSelection == JFileChooser.APPROVE_OPTION) {
			File fileToSave = fileChooser.getSelectedFile();
			String caminho = fileToSave.getAbsolutePath();

			try {
				ImageIO.write(carregarItem, "PNG", new File(caminho + ".png"));
			} catch (IOException ex) {
				Logger.getLogger(FrmPrincipal.class.getName()).log(Level.SEVERE, null, ex);
			}
			System.out.println("Salvo Em: " + fileToSave.getAbsolutePath());
		}

	}

	public void imagemSincronizada(File path) {
		manipulador = new CarregaMetodos();

		imagemAtual = path.getPath().replace("\\", "\\\\");

		srcImage = JAI.create("fileload", imagemAtual);

		contentPane = ImageFrame.getContentPane();
		contentPane.setLayout(new BorderLayout());

		if (flag == false) {
			// imagem sincronizada

			imagem = new DisplayTwoSynchronizedImages(srcImage, srcImage);
			ImageFrame.setVisible(true);
			contentPane.add(imagem);
			flag = true;
		} else {
			contentPane.remove(imagem);
			imagem = new DisplayTwoSynchronizedImages(srcImage, srcImage);
			ImageFrame.setVisible(true);
			contentPane.add(imagem);
		}
		try {
			lblPixelBrancos.setText(manipulador.dimensõesImagem(imagemAtual));
			lblQuantidadeBrancos
					.setText("Quantidade de Pixel Brancos: " + Integer.toString(manipulador.contarPixel(imagemAtual)));
		} catch (IOException | IllegalArgumentException ev) {
			ev.printStackTrace();
			throw new IllegalArgumentException("Erro ao carregar itens");
		}
		setVisible(true); // show the frame.
	}

	private void carregaItens(CarregaMetodos manipulador, int escolha) {
		try {
			switch (escolha) {
			case 1:

				carregarItem = manipulador.suavizarImagem(imagemAtual);
				ImageFrame.setTitle("Suavizar Imagem");

				break;

			case 2:
				carregarItem = manipulador.bordasHorizontais(imagemAtual);
				ImageFrame.setTitle("Bordas Horizontais");
				break;

			case 3:
				carregarItem = manipulador.ditalarImagem(imagemAtual, 1);
				ImageFrame.setTitle("Dilatar Imagem");
				break;
			case 4:
				carregarItem = manipulador.ditalarImagem(imagemAtual, 2);
				ImageFrame.setTitle("Eroditar Imagem");
				break;
			case 5:
				carregarItem = manipulador.ihs(imagemAtual);
				ImageFrame.setTitle("Imagem IHS");
				break;
			case 6:
				carregarItem = manipulador.inverterImagem(imagemAtual);
				ImageFrame.setTitle("Inverter Imagem");
				break;
			case 7:
				carregarItem = manipulador.rotacionaImagem(imagemAtual, rotacao);

				break;
			case 8:
				carregarItem = manipulador.escalaImagem(imagemAtual, rotacao);

				break;
			case 9:
				imagemSincronizada(file);
				return;
			default:
				break;
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(FrmPrincipal.this, "Erro ao carregar Item \nStacktrace do erro: " + e.getMessage(),
					"Ocorreu um erro", JOptionPane.ERROR_MESSAGE);
		}

		ImageFrame.dispose();
		contentPane.remove(imagem);
		imagem = new DisplayTwoSynchronizedImages(srcImage, carregarItem);
		contentPane.add(imagem);
		ImageFrame.setVisible(true);

	}
}
