package ImagemSubstitutaUsandoJAI;

import com.sun.media.jai.widget.DisplayJAI;
import java.awt.image.DataBuffer;
import java.awt.image.renderable.ParameterBlock;
import javax.media.jai.JAI;
import javax.media.jai.PlanarImage;

/**
 *
 * @author 21452-8
 */
public class DisplaySurrogateImage extends DisplayJAI {

    protected PlanarImage surrogateImage;
    protected int width, height;

    public DisplaySurrogateImage(PlanarImage image) {

        width = image.getWidth();
        height = image.getHeight();

        // Recuperamos valores externos da image.
        ParameterBlock pbMaxMin = new ParameterBlock();
        pbMaxMin.addSource(image);
        PlanarImage extrema = JAI.create("extrema", pbMaxMin);

        double[] allMins = (double[]) extrema.getProperty("minimum");
        double[] allMaxs = (double[]) extrema.getProperty("maximum");

        double minValue = allMins[0];
        double maxValue = allMaxs[0];

        for (int v = 1; v < allMins.length; v++) {
            if (allMins[v] < minValue) {
                minValue = allMins[v];
            }
            if (allMaxs[v] > maxValue) {
                maxValue = allMaxs[v];
            }
        }

        //Reescalamos os níveis de cinza da imagem.
        double[] substract = new double[1];
        substract[0] = minValue;
        double[] multiplyBy = new double[1];
        multiplyBy[0] = 255. / (maxValue - minValue);

        ParameterBlock pbSub = new ParameterBlock();
        pbSub.addSource(image);
        pbSub.add(substract);
        surrogateImage = (PlanarImage) JAI.create("subtractconst", pbSub);

        ParameterBlock pbMult = new ParameterBlock();
        pbMult.addSource(surrogateImage);
        pbMult.add(multiplyBy);
        surrogateImage = (PlanarImage) JAI.create("multiplyconst", pbMult);

        //Convertemos para bytes
        ParameterBlock pbConvert = new ParameterBlock();
        pbConvert.addSource(surrogateImage);
        pbConvert.add(DataBuffer.TYPE_BYTE);
        surrogateImage = JAI.create("format", pbConvert);

        //Usamos esta imagem para display
        set(surrogateImage);

    }
    
 

}
